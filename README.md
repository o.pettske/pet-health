# Some project
###  Initialization Guide
1. Check if virtualization is enabled.
2. Install [VirtualBox 6.1.22](https://www.virtualbox.org/wiki/Downloads).
3. Install [Vagrant 2.2.15](https://www.vagrantup.com/downloads).
4. Reload computer.
5. In root directory (some-project in our case) run: 
`vagrant plugin install vagrant-hostsupdater`
6. Go to etc/hosts (*Windows: c:\windows\system32\drivers\etc\hosts*) and add: `192.168.20.20 pethealth.com.loc www.pethealth.com.loc`
7. In root directory run: `vagrant up`
8. `vagrant ssh`