#!/bin/bash

# Install OracleJDK
sudo mkdir -p /usr/java/oracle
cd /usr/java/oracle
sudo cp /var/www/pethealth.com.loc/jdk-16.0.1_linux-x64_bin.tar.gz jdk-16.0.1_linux-x64_bin.tar.gz
sudo tar -xzvf jdk-16.0.1_linux-x64_bin.tar.gz

# Update Profile
profile_settings="{
  # Java 16
  JAVA_HOME=/usr/java/oracle/jdk-16.0.1
  PATH=$PATH:$HOME/bin:$JAVA_HOME/bin
  export JAVA_HOME
  export PATH
}"

sudo echo "$profile_settings" >> /etc/profile
