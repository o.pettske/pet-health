#!/bin/bash

# Install Apache Maven
sudo tar xf /var/www/pethealth.com.loc/apache-maven-3.8.1-bin.tar.gz -C /opt
sudo ln -s /opt/apache-maven-3.8.1 /opt/maven

profile_settings="{
  export JAVA_HOME=/usr/lib/jvm/default-java
  export M2_HOME=/opt/maven
  export MAVEN_HOME=/opt/maven
  export PATH=${M2_HOME}/bin:${PATH}
}"

sudo echo "$profile_settings" >> /etc/profile.d/maven.sh
sudo chmod +x /etc/profile.d/maven.sh
source /etc/profile.d/maven.sh
